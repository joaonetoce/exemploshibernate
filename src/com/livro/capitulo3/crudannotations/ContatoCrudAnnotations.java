package com.livro.capitulo3.crudannotations;

import java.sql.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.livro.capitulo3.conexao.HibernateUtil;

public class ContatoCrudAnnotations {

	public void salvar(ContatoAnnotations contatoAnnotations){
		Session sessao = null;
		Transaction transacao = null;
		try {
			sessao = HibernateUtil.getSessionFactory().openSession();
			transacao = sessao.beginTransaction();
			sessao.save(contatoAnnotations);
			transacao.commit();
		} catch (HibernateException e) {
			System.out.println("Não foi possível inserir o contato. Erro: " + e.getMessage());
		}finally{
			try{
				sessao.close();
			}catch(Throwable e){
				System.out.println("Erro ao fechar operação de inserção. Mensagem: " + e.getMessage());
			}
		}
	}
	
	public void atualizar(ContatoAnnotations contatoAnnotations){
		Session sessao = null;
		Transaction transacao = null;
		try {
			sessao = HibernateUtil.getSessionFactory().openSession();
			transacao = sessao.beginTransaction();
			sessao.update(contatoAnnotations);
			transacao.commit();
		} catch (HibernateException e) {
			System.out.println("Não foi possível alterar o contato. Erro " + e.getMessage());
		}finally{
			try {
				sessao.close();
			} catch (Throwable e) {
				System.out.println("Erro ao fechar a operação de atualização. Mensagem: " + e.getMessage());
			}
		}
	}
	
	public void excluir(ContatoAnnotations contatoAnnotations){
		Session sessao = null;
		Transaction transacao = null;
		try {
			sessao = HibernateUtil.getSessionFactory().openSession();
			transacao = sessao.beginTransaction();
			sessao.delete(contatoAnnotations);
			transacao.commit();
		} catch (HibernateException e) {
			System.out.println("Não foi possível excluir o contato. Erro: " + e.getMessage());
		}finally{
			try {
				sessao.close();
			} catch (Throwable e) {
				System.out.println("Erro ao fechar operação de exclusão. Mensagem: " + e.getMessage());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ContatoAnnotations> listar(){
		Session sessao = null;
		Transaction transacao = null;
		Query consulta = null;
		List<ContatoAnnotations> resultado = null;
		try {
			sessao = HibernateUtil.getSessionFactory().openSession();
			transacao = sessao.beginTransaction();
			consulta = sessao.createQuery("from Contato");
			resultado = consulta.list();
			transacao.commit();
			return resultado;
		} catch (HibernateException e) {
			System.out.println("Não foi possível selecionar contatos. Erro: " + e.getMessage());
			throw new HibernateException(e);
		}finally{
			try {
				sessao.close();
			} catch (Throwable e) {
				System.out.println("Erro ao fechar operação de consulta. Mensagem: " + e.getMessage());
			}
		}
	}
	
	public ContatoAnnotations buscaContato(int valor){
		ContatoAnnotations contatoAnnotations = null;
		Session sessao = null;
		Transaction transacao = null;
		Query consulta = null;
		try {
			sessao = HibernateUtil.getSessionFactory().openSession();
			transacao = sessao.beginTransaction();
			consulta = sessao.createQuery("from ContatoAnnotations where codigo = :parametro");
			consulta .setInteger("parametro", valor);
			contatoAnnotations = (ContatoAnnotations) consulta.uniqueResult();
			transacao.commit();
			return contatoAnnotations;
		} catch (HibernateException e) {
			System.out.println("Não foi possível buscar contato. Erro: " + e.getMessage());
		}finally{
			try {
				sessao.close();
			} catch (Throwable e) {
				System.out.println("Erro ao fechar operação de buscar. Mensagem: " + e.getMessage());
			}
		}
		return contatoAnnotations;
	}	
	
	public static void main(String[] args) {
		ContatoCrudAnnotations contatoCrudAnnotations = new ContatoCrudAnnotations();
		String[] nomes = {"Solanu", "Lunare", "Venusiana"};
		String[] fones = {"(47) 3333-4444", "(47) 7777-5555", "(47) 9090-2525"};
		String[] emails = {"solanu@teste.com.br", "lunare@teste.com.br", "venusiana@teste.com.br"};
		String[] observacoes = {"Novo cliente", "Cliente em dia", "Ligar na sexta"};
		ContatoAnnotations contatoAnnotations = null;
		
		//Adicionar contatos
		/*for (int i = 0; i < nomes.length; i++) {
			contatoAnnotations = new ContatoAnnotations();
			contatoAnnotations.setNome(nomes[i]);
			contatoAnnotations.setTelefone(fones[i]);
			contatoAnnotations.setEmail(emails[i]);
			contatoAnnotations.setDataCadastro(new Date(System.currentTimeMillis()));
			contatoAnnotations.setObservacao(observacoes[i]);
			contatoCrudAnnotations.salvar(contatoAnnotations);
		}*/
		
		//Excluir contato por ID
		/*contato = contatoCrudXML.buscaContato(1);
		contatoCrudXML.excluir(contato);*/
		
		//Exibir contato
		contatoAnnotations = contatoCrudAnnotations.buscaContato(5);		
		System.out.println("Nome: " + contatoAnnotations.getNome());
		System.out.println("Telefone: " + contatoAnnotations.getTelefone());
		System.out.println("Email: " + contatoAnnotations.getEmail());
		System.out.println("Data de Cadastro: " + contatoAnnotations.getDataCadastro());
		System.out.println("Observação: " + contatoAnnotations.getObservacao());
		
		System.out.println("Total de registros cadastrados: " + contatoCrudAnnotations.listar().size());
	}
}
